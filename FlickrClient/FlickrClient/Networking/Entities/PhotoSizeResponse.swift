//
//  PhotoSizeResponse.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

struct PhotoSizeResponse: Codable {
    let container: PhotoSizeContainer
    
    enum CodingKeys: String, CodingKey {
        case container = "sizes"
    }
}

struct PhotoSizeContainer: Codable {
    let sizes: [PhotoSize]
    
    enum CodingKeys: String, CodingKey {
        case sizes = "size"
    }
}

struct PhotoSize: Codable {
    let label: PhotoSizeLabel
    let source: URL
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        source = try container.decode(URL.self, forKey: CodingKeys.source)
        label = (try? container.decode(PhotoSizeLabel.self, forKey: CodingKeys.label)) ?? .UNDEFINED
    }
}

enum PhotoSizeLabel: String, Codable, Equatable {
    case Square
    case LargeSquare = "Large Square"
    case Thumbnail
    case Small
    case Small320 = "Small 320"
    case Medium
    case Medium640 = "Medium 640"
    case Medium800 = "Medium 800"
    case Large
    case Large1600 = "Large 1600"
    case UNDEFINED
}

struct PhotoUrlContainer {
    let regular: URL
    let large: URL
}
