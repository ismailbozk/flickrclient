//
//  Photo.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

struct PhotoResponse: Codable {
    let container: PhotoContainer
    
    enum CodingKeys : String, CodingKey {
        case container = "photos"
    }
}

struct PhotoContainer: Codable {
    let photos: [Photo]
    
    enum CodingKeys : String, CodingKey {
        case photos = "photo"
    }
}

struct Photo: Codable {
    let id: String
}
