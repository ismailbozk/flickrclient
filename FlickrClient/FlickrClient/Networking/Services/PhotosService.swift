//
//  PhotosService.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

final class PhotosService {
    
    private let client: NetworkingClient
    private let baseUrl: URL
    
    init(client: NetworkingClient, baseUrl: URL = Config.flickrBaseUrl) {
        self.client = client
        self.baseUrl = baseUrl
    }
    
    
    func getPhotos(for keyWord: String, page: Int, _ completion: @escaping (Result<[Photo], NetworkingError>) -> Void) -> NetworkingTask {

        let request = photosRequest(with: keyWord, page: page)
        return client.request(request, { (result: Result<PhotoResponse, NetworkingError>) in
            switch result {
            case .success(let response):
                completion(.success(response.container.photos))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    private func photosRequest(with searchTag: String, page: Int) -> URLRequest {
        guard var urlComponents = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true) else {
            assertionFailure("Couldn't create photos request!")
            return URLRequest(url: baseUrl)
        }

        let methodQuery = URLQueryItem(name: ApiConstants.Query.methodKey, value: ApiConstants.Query.searchMethodValue)
        let apiKeyQuery = URLQueryItem(name: ApiConstants.Query.apiKey, value: Config.flickrApiKey)
        let tagQuery = URLQueryItem(name: ApiConstants.Query.tagsKey, value: searchTag)
        let pageQuery = URLQueryItem(name: ApiConstants.Query.pageKey, value: String(page))
        let formatQuery = URLQueryItem(name: ApiConstants.Query.formatKey, value: ApiConstants.Query.formatValue)
        let noJsonQuery = URLQueryItem(name: ApiConstants.Query.noJsonKey, value: ApiConstants.Query.noJsonValue)
        
        urlComponents.queryItems = [methodQuery, apiKeyQuery, tagQuery, pageQuery, formatQuery, noJsonQuery]
        
        guard let url = urlComponents.url else {
            assertionFailure("Couldn't create photos request!")
            return URLRequest(url: baseUrl)
        }
        
        return URLRequest(url: url)
    }
    
    func getPhotoSize(with photoId: String, completion: @escaping (Result<[PhotoSize], NetworkingError>) -> Void) -> NetworkingTask {
        
        let request = photoSizeRequest(with: photoId)
        return client.request(request, { (result: Result<PhotoSizeResponse, NetworkingError>) in
            switch result {
            case .success(let response):
                completion(.success(response.container.sizes))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    private func photoSizeRequest(with photoId: String) -> URLRequest {
        guard var urlComponents = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true) else {
            assertionFailure("Couldn't create photo size request!")
            return URLRequest(url: baseUrl)
        }
        
        let methodQuery = URLQueryItem(name: ApiConstants.Query.methodKey, value: ApiConstants.Query.sizeMethodValue)
        let apiKeyQuery = URLQueryItem(name: ApiConstants.Query.apiKey, value: Config.flickrApiKey)
        let formatQuery = URLQueryItem(name: ApiConstants.Query.formatKey, value: ApiConstants.Query.formatValue)
        let noJsonQuery = URLQueryItem(name: ApiConstants.Query.noJsonKey, value: ApiConstants.Query.noJsonValue)
        let photoIdQuery = URLQueryItem(name: ApiConstants.Query.photoIdKey, value: photoId)
        
        urlComponents.queryItems = [methodQuery, apiKeyQuery, formatQuery, noJsonQuery, photoIdQuery]
        
        guard let url = urlComponents.url else {
            assertionFailure("Couldn't create photo size request!")
            return URLRequest(url: baseUrl)
        }
        
        return URLRequest(url: url)

    }
}
