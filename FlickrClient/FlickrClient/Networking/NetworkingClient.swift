//
//  NetworkingClient.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

struct ApiResponse {
    let httpResponse: HTTPURLResponse
    let data: Data
}

/// The interface for the netwoking client so we can have more than one implementation or test the other component without depending on real netwrking api client.
protocol NetworkingClient {
    func request<T: Decodable>(_ request: URLRequest, _ completion: @escaping (Result<T, NetworkingError>) -> Void) -> NetworkingTask
}

/// NetworkingTask interface which is used as the return type of networking client to perform operations like canceling an ongoing call.
protocol NetworkingTask: AnyObject {
    func cancelCall()
}

enum NetworkingError: Error {
    case networkError(error: Error)
    case apiError(response: ApiResponse)
    case invalidResponse
}
