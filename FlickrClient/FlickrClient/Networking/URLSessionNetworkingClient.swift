//
//  URLSessionNetworkingClient.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

final class URLSessionNetworkingClient: NetworkingClient {
    private let session: URLSession
    private let decoder: JSONDecoder
    
    /// Api client to perform networking tasks and get (if defined) the requested JSON as Model struct
    ///
    /// - Parameters:
    ///   - urlSession: session, default _URLSession(configuration: URLSessionConfiguration.default)_
    ///   - decoder: json parser to decode the incoming json model to requested model struct, default _JSONDecoder()_
    init(urlSession: URLSession = URLSession(configuration: URLSessionConfiguration.default),
         decoder: JSONDecoder = JSONDecoder()) {
        self.session = urlSession
        self.decoder = decoder
    }
    
    /// Api client to perform networking tasks and get (if defined) the requested JSON as Model struct
    ///
    /// - Parameters:
    ///   - request: request model for the request
    ///   - completion: async completion handler
    /// - Returns: returns the network task, checkout _NetworkingTask_ for more info
    @discardableResult
    func request<T: Decodable>(_ request: URLRequest, _ completion: @escaping (Result<T, NetworkingError>) -> Void) -> NetworkingTask {
        let task = session.dataTask(with: request) { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            guard error == nil else {
                DispatchQueue.main.async {
                    completion(.failure(.networkError(error: error!)))
                }
                return
            }
            
            guard let data = data, let httpResponse = response as? HTTPURLResponse else {
                DispatchQueue.main.async {
                    completion(.failure(.invalidResponse))
                }
                return
            }
            
            let result: Result<T, NetworkingError>
            switch httpResponse.statusCode {
            case 200...299:
                do {
                    result = try .success(self.decoder.decode(T.self, from: data))
                }
                catch {
                    result = .failure(.invalidResponse)
                }

            default:
                let apiResponse = ApiResponse(httpResponse: httpResponse, data: data)
                result = .failure(.apiError(response: apiResponse))
            }
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
        task.resume()
        
        return task
    }
}

extension URLSessionDataTask: NetworkingTask {
    func cancelCall() {
        self.cancel()
    }
}
