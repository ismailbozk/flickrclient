//
//  Config.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

final class Config {
    static var flickrBaseUrl: URL {
        return URL(string: infoForKey("FlickrBackEndUrl")!)!
    }
    
    static var flickrApiKey: String {
        return infoForKey("FlickrApiKey")!
    }
    
    private class func infoForKey(_ key: String) -> String? {
        return (Bundle.main.infoDictionary?[key] as? String)?.replacingOccurrences(of: "\\", with: "")
    }
}
