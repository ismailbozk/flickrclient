//
//  Constants.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

struct ApiConstants {

    struct Query {
        static let methodKey = "method"
        static let searchMethodValue = "flickr.photos.search"
        static let sizeMethodValue = "flickr.photos.getSizes"
        static let apiKey = "api_key"
        static let tagsKey = "tags"
        static let pageKey = "page"
        static let formatKey = "format"
        static let formatValue = "json"
        static let noJsonKey = "nojsoncallback"
        static let noJsonValue = "1"
        static let photoIdKey = "photo_id"
    }
}

