//
//  HomeInteractor.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

protocol HomeInteractor {
    func getPhotos(for tag: String, page: Int)
    func getSizes(for photoId: String, completion: @escaping (Result<PhotoUrlContainer, Error>) -> Void)
    
}
