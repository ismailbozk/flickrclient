//
//  DefaultHomeInteractor.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

final class DefaultHomeInteractor: HomeInteractor {
    private let service: PhotosService
    weak var presenter: HomePresenter?
    
    private var latestFetchOperation: (tag: String, task: NetworkingTask)? = nil
    
    init(photosService: PhotosService) {
        self.service = photosService
    }
    
    func getPhotos(for tag: String, page: Int) {
        // since there is a new search tag, terminate the previous search call 
        if let latestFetchOperation = latestFetchOperation,
            latestFetchOperation.tag != tag {
            print("Search operation cancelled due to new search is in progress")
            latestFetchOperation.task.cancelCall()
        }
        
        
        let task = service.getPhotos(for: tag, page: page) { [weak self] (result: Result<[Photo], NetworkingError>) in
            guard let self = self else { return }
            
            switch result {
            case .success(let photos):
                self.presenter?.fetched(photos)
            case .failure(let error):
                self.presenter?.handlePageLoadError(error)
            }
        }
        
        latestFetchOperation = (tag, task)
    }
    
    func getSizes(for photoId: String, completion: @escaping (Result<PhotoUrlContainer, Error>) -> Void) {
        service.getPhotoSize(with: photoId) { [weak self] (result: Result<[PhotoSize], NetworkingError>) in
            guard let self = self else { return }

            switch result {
            case .success(let sizes):
                if let squareSize = sizes.first(where: { $0.label == .LargeSquare }),
                    let largeSize = sizes.first(where: { $0.label == .Large }) {
                    let urlContainer = PhotoUrlContainer(regular: squareSize.source, large: largeSize.source)
                    completion(.success(urlContainer))
                }
                else {
                    let error = NSError(domain: String(describing: self), code: 1004, userInfo: [NSLocalizedDescriptionKey: "IMAGE_NOT_FOUND"])
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
