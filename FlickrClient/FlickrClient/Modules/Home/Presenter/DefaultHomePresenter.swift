//
//  DefaultHomePresenter.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

final class DefaultHomePresenter {

    private let interactor: HomeInteractor
    weak var view: HomeView?
    
    private var display = HomeDisplay(photos: [])
    private var searchQuery: SearchQueryModel = SearchQueryModel(tag: "")
    private var isPageLoading = false
    
    private let queue = DispatchQueue(label: "Photo Display Access Queue", qos: DispatchQoS.userInteractive)
    
    init(interactor: HomeInteractor) {
        self.interactor = interactor
    }
}

extension DefaultHomePresenter: HomePresenter {    

    func fetched(_ photos: [Photo]) {
        isPageLoading = false
        
        queue.async {
            self.display.add(photos.map({PhotoDisplay(id: $0.id , photoUrl: .notPresent, largePhotoUrl: .notPresent)}))
            DispatchQueue.main.async {
                self.view?.updatePhotos(with: self.display)
            }
        }
        
        if photos.count == 0 {
            searchQuery.allPagesFetched = true
        }
        
        view?.stopLoading()
    }
    
    func fetched(_ urlContainer: PhotoUrlContainer, for photoId: String, at index: Int) {
        display.updatePhoto(at: index, regularPhotoUrl: urlContainer.regular, largePhotoUrl: urlContainer.large)
        view?.imageUrlFetchedForPhotoDisplay(at: index, in: display)
    }
    
    func fecthSizeFailed(for photoId: String, at index: Int) {
        display.updateFailedPhoto(at: index)
        view?.imageUrlFetchedForPhotoDisplay(at: index, in: display)
    }
    
    func handlePageLoadError(_ error: Error) {
        isPageLoading = false

        view?.display(error: error)
        
        view?.stopLoading()
    }
    
    func handleImageSizeFetchError(_ error: Error) {
        view?.display(error: error)
    }
}

extension DefaultHomePresenter: HomeEventHandler {
    func userDidSearch(tag: String) {
        
        searchQuery = SearchQueryModel(tag: tag)
        display = HomeDisplay(photos: [])
        view?.updatePhotos(with: display)
        
        guard !tag.isEmpty else {
            return
        }

        isPageLoading = true
        
        interactor.getPhotos(for: tag, page: searchQuery.latestFetchedPage)
        
        view?.startLoading()
    }
    
    func userDidScrollToBottom() {
        // if all items fetched then do additional calls
        guard !searchQuery.allPagesFetched else {
            return
        }
        
        searchQuery.latestFetchedPage += 1
        isPageLoading = true
        interactor.getPhotos(for: searchQuery.tag, page: searchQuery.latestFetchedPage)
        view?.startLoading()
    }
    
    func userDidScrollToPhoto(id: String, at index: Int) {
        guard display.photos[index].photoUrl == .notPresent else {
            return
        }
        display.photos[index].photoUrl = .downloading
        interactor.getSizes(for: id) { [weak self] (result) in
            switch result {
            case .success(let size):
                self?.fetched(size, for: id, at: index)
            case .failure:
                self?.fecthSizeFailed(for: id, at: index)
            }
        }
    }
}
