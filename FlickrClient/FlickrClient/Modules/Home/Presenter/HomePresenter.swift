//
//  HomePresenter.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

protocol HomePresenter: AnyObject {
    func fetched(_ photos: [Photo])
    func handlePageLoadError(_ error: Error)
    func handleImageSizeFetchError(_ error: Error)
}
