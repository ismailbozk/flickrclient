//
//  SearchQueryModel.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

struct SearchQueryModel {
    let tag: String
    var latestFetchedPage: Int = 1
    var allPagesFetched = false
    
    init(tag: String, latestFetchedPage: Int = 1, allPagesFetched: Bool = false) {
        self.tag = tag
        self.latestFetchedPage = latestFetchedPage
        self.allPagesFetched = allPagesFetched
    }
}
