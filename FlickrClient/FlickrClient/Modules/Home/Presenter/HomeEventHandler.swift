//
//  HomeEventHandler.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

protocol HomeEventHandler: AnyObject {
    func userDidSearch(tag: String)
    func userDidScrollToBottom()
    func userDidScrollToPhoto(id: String, at index: Int)
}
