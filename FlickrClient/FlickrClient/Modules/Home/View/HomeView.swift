//
//  HomeView.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

protocol HomeView: AnyObject {
    func updatePhotos(with display: HomeDisplay)
    func display(error: Error)
    func imageUrlFetchedForPhotoDisplay(at index: Int, in display: HomeDisplay)
    func startLoading()
    func stopLoading()
}
