//
//  ImageZoomView.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

class ImageZoomView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var doneButton: UIButton! {
        didSet {
            doneButton.setTitle(NSLocalizedString("DONE", comment: "Done"), for: .normal)
        }
    }
}
