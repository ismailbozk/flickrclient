//
//  HomeDisplay.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

struct HomeDisplay {
    var photos: [PhotoDisplay]
    
    mutating func add(_ photos: [PhotoDisplay]) {
        self.photos += photos
    }

    mutating func updatePhoto(at index: Int, regularPhotoUrl: URL, largePhotoUrl: URL) {
        photos[index].photoUrl = .downloaded(url: regularPhotoUrl)
        photos[index].largePhotoUrl = .downloaded(url: largePhotoUrl)
    }

    mutating func updateFailedPhoto(at index: Int) {
        photos[index].photoUrl = .failed
        photos[index].largePhotoUrl = .failed
    }
}

struct PhotoDisplay {
    let id: String
    var photoUrl: PhotoUrlStatus
    var largePhotoUrl: PhotoUrlStatus
}

enum PhotoUrlStatus: Equatable {
    case notPresent
    case downloading
    case downloaded(url: URL)
    case failed
}
