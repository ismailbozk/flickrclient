//
//  PhotoCollectionViewCell.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit
import Kingfisher

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var imageView: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageView.image = UIImage(named: "placeholder")
    }
    
    func updateImage(with url: URL) {
        imageView.kf.setImage(with: url, options: [.transition(.fade(0.2))])
    }
}



