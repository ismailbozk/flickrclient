//
//  SearchCollectionHeaderView.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

class SearchCollectionHeaderView: UICollectionReusableView {
        
    @IBOutlet weak var searchBar: UISearchBar!

}
