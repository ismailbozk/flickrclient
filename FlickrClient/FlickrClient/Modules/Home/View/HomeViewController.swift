//
//  HomeViewController.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit
import Kingfisher

class HomeViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.sectionFootersPinToVisibleBounds = true
        }
    }
    
    var eventHandler: HomeEventHandler?
    
    private var display: HomeDisplay?

    private var footerView: HomeLoadingFooterView?
    
    private var zoomView = ImageZoomView.instantiateFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupZoomView()
    }
    
    private func setupZoomView() {
        guard let zoomView = zoomView else {
            return
        }
        zoomView.translatesAutoresizingMaskIntoConstraints = false
        zoomView.doneButton.addTarget(self, action: #selector(closeZoomView), for: .touchUpInside)
        view.addSubview(zoomView)
        
        zoomView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        zoomView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        zoomView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        zoomView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        zoomView.alpha = 0
        zoomView.isHidden = true
    }
    
    @objc private func closeZoomView() {
        UIView.animate(withDuration: 0.2, animations: {
            self.zoomView?.alpha = 0
        }) { (finished) in
            if finished {
                self.zoomView?.isHidden = true
            }
        }
    }
    
    private func openZoomView() {
        zoomView?.alpha = 0
        zoomView?.isHidden = false
        
        UIView.animate(withDuration: 0.2, animations: {
            self.zoomView?.alpha = 1
        }) { (finished) in
            
        }
    }
}

extension HomeViewController: HomeView {
    
    func updatePhotos(with display: HomeDisplay) {
        self.display = display
        collectionView.reloadData()
    }
    
    func display(error: Error) {
        let alertController = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: NSLocalizedString("CLOSE", comment: "Close"), style: UIAlertAction.Style.default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(closeAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func imageUrlFetchedForPhotoDisplay(at index: Int, in display: HomeDisplay) {
        self.display = display
        
        guard let cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0)) as? PhotoCollectionViewCell,
            case .downloaded(let url) = display.photos[index].photoUrl else {
                return
        }
        
        cell.updateImage(with: url)
    }
    
    func startLoading() {
        self.footerView?.indicator.startAnimating()
    }
    
    func stopLoading() {
        self.footerView?.indicator.stopAnimating()
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return display?.photos.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let display = display,
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PhotoCollectionViewCell.self),
                                                          for: indexPath) as? PhotoCollectionViewCell else {
                                                            
            return UICollectionViewCell()
        }
        
        switch display.photos[indexPath.item].photoUrl {
        case .downloaded(let url):
            cell.updateImage(with: url)
        default:
            //do not handle here
            break
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let display = display else { return }
    
        eventHandler?.userDidScrollToPhoto(id: display.photos[indexPath.item].id, at: indexPath.item)
        
        if display.photos.count - 1 == indexPath.item {
            //load the next page
            eventHandler?.userDidScrollToBottom()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionHeader),
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: SearchCollectionHeaderView.self), for: indexPath) as? SearchCollectionHeaderView {
            
            headerView.searchBar.delegate = self
            return headerView
        }
        else if (kind == UICollectionView.elementKindSectionFooter),
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: HomeLoadingFooterView.self), for: indexPath) as? HomeLoadingFooterView {
            self.footerView = footerView
            return footerView
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let display = display,
            case .downloaded(let url) = display.photos[indexPath.item].largePhotoUrl,
            let cell = collectionView.cellForItem(at: indexPath) else {
                return
        }
        let placeholder = UIImage(named: "placeholder")
        zoomView?.imageView.kf.setImage(with: url, placeholder: placeholder)
        
        openZoomView()
    }
}


extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = collectionView.bounds.width
        let padding: CGFloat = 10.0
        let width: CGFloat = (screenWidth - 3 * padding) / 2
        return CGSize(width: width, height: width)
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        eventHandler?.userDidSearch(tag: searchBar.text ?? "")
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
