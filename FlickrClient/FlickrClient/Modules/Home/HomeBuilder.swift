//
//  HomeBuilder.swift
//  FlickrClient
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

final class HomeBuilder {
    func build() -> UINavigationController {
        
        let client = URLSessionNetworkingClient()
        let service = PhotosService(client: client)
    
        let interactor = DefaultHomeInteractor(photosService: service)
        let presenter = DefaultHomePresenter(interactor: interactor)
        interactor.presenter = presenter
        
        let view = viewController
        view.eventHandler = presenter
        presenter.view = view
        
        let navigationController = UINavigationController(rootViewController: view)
        return navigationController
    }
    
    private var viewController: HomeViewController {
        return storyboard.instantiateViewController(withIdentifier: String(describing: HomeViewController.self)) as! HomeViewController
    }

    private var storyboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
