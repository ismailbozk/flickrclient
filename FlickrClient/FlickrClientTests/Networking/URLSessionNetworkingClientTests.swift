//
//  URLSessionNetworkingClientTests.swift
//  FlickrClientTests
//
//  Created by Bozkurt on 9/11/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import XCTest

class URLSessionNetworkingClientTests: XCTestCase {
    private var networkingClient: URLSessionNetworkingClient?
    private var mockSession: MockURLSession?

    func testSuccess() {
        let mockResponse = SomeResponse(some_string: "string", some_int: 1, some_double: 2.0)
        let data = try! JSONEncoder().encode(mockResponse)
        let url = URL(string: "http://www.google.com/get/some/info")!
        let httpResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: "1.1", headerFields: nil)

        mockSession = MockURLSession(data: data, httpResponse: httpResponse,  error: nil)
        networkingClient = URLSessionNetworkingClient(urlSession: mockSession!)
        
        let expectation = XCTestExpectation(description: "Happy Path")
        
        let request = URLRequest(url: url)
        
        networkingClient?.request(request) { (result: Result<SomeResponse, NetworkingError>) in
            switch result {
            case .success:
                XCTAssertTrue(true, "Result should have succeeded!")
            case .failure:
                XCTAssertTrue(false, "Result should have succeeded!")
            }
            
            expectation.fulfill()
        }
        
        
        wait(for: [expectation], timeout: 60)
    }
    
    func testNetworkError() {
        let error = NSError(domain: String(describing: URLSessionNetworkingClientTests.self),
                            code: -666,
                            userInfo: [NSLocalizedDescriptionKey: "GENERIC_ERROR"])
        mockSession = MockURLSession(data: nil, httpResponse: nil, error: error)
        networkingClient = URLSessionNetworkingClient(urlSession: mockSession!)
        
        let expectation = XCTestExpectation(description: "Error Path")
        
        let request = URLRequest(url: URL(string: "http://www.google.com/get/some/info")!)
        
        networkingClient?.request(request) { (result: Result<SomeResponse, NetworkingError>) in
            switch result {
            case .success:
                XCTAssertTrue(false, "Result should have failed!")
            case .failure:
                XCTAssertTrue(true, "Result should have failed!")
            }
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 60)
    }
    
    func testApiError() {
        let url = URL(string: "http://www.google.com/get/some/info")!
        let response = HTTPURLResponse(url: url, statusCode: 422, httpVersion: "1.1", headerFields: nil)
        mockSession = MockURLSession(data: Data(), httpResponse: response, error: nil)
        networkingClient = URLSessionNetworkingClient(urlSession: mockSession!)
        
        let expectation = XCTestExpectation(description: "Api Error Path")
        
        let request = URLRequest(url: URL(string: "http://www.google.com/get/some/info")!)
        
        networkingClient?.request(request) { (result: Result<SomeResponse, NetworkingError>) in
            switch result {
            case .success:
                XCTAssertTrue(false, "Result should have failed!")
            case .failure(let error):
                switch error {
                case .networkError:
                    XCTAssertTrue(false, "Result should have failed as api error!")
                case .apiError(let response):
                    XCTAssertTrue(true, "Result should have failed as api error!")
                    XCTAssertTrue(response.httpResponse.statusCode == 422)
                case .invalidResponse:
                    XCTAssertTrue(false, "Result should have failed as api error!")
                }
            }
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 60)
    }
}



fileprivate final class MockNetworkingTask: NetworkingTask {
    func cancelCall() {
        // cancel here
    }
}

fileprivate struct SomeResponse: Codable {
    let some_string: String
    let some_int: Int
    let some_double: Double
}

fileprivate final class MockURLSession: URLSession {
    private let error: Error?
    private let httpResponse: HTTPURLResponse?
    private let data: Data?
    init(data: Data?, httpResponse: HTTPURLResponse?, error: Error?) {
        self.data = data
        self.error = error
        self.httpResponse = httpResponse
        super.init()
    }
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            completionHandler(self.data, self.httpResponse, self.error)
        }
        
        return URLSessionDataTask()
    }
}
